
import javax.swing.*;

import java.awt.Color;
import java.awt.Font;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Clipboard;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class Editor extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel optionPanel = new JPanel();
	private JPanel tilePanel = new JPanel();
	
	private ImageIcon selectedIcon;
	private char selectedChar;
	private Level l = new Level();
	
	private final int OPTIONS_AMOUNT = 7;
	private JLabel options[] = new JLabel[OPTIONS_AMOUNT];
	private JLabel tiles[][] = new JLabel[Level.COLS][Level.ROWS];
	
	private ImageIcon[] icons = {
			new ImageIcon(getClass().getResource("empty.png")),
			new ImageIcon(getClass().getResource("wall.png")),
			new ImageIcon(getClass().getResource("pacman.png")),
			new ImageIcon(getClass().getResource("small_thing.png")),
			new ImageIcon(getClass().getResource("thing.png")),
			new ImageIcon(getClass().getResource("ghost.png")),
			new ImageIcon(getClass().getResource("door.png"))
	};
	
	private char[] characters = {
			' ',
			'#',
			'P',
			'.',
			'O',
			'F',
			'-'
	};
	
	// put into a class?
	private class CustomMouseAdapter extends MouseAdapter {
		
		int row, col;
		
		public CustomMouseAdapter(int col, int row) {
			this.row = row;
			this.col = col;
		}
		
		public void mouseEntered(MouseEvent evt) {
			if(selectedIcon != null)
				tiles[col][row].setIcon(selectedIcon);
			
			// if the mouse is already pressed when it enters
			if (evt.getModifiers() == MouseEvent.BUTTON1_MASK)
				l.insert(col, row, selectedChar);
		}

		public void mouseExited(MouseEvent evt) {
			// if the mouse is pressed when it exits, it will still "paint"
			if (evt.getModifiers() == MouseEvent.BUTTON1_MASK)
				l.insert(col, row, selectedChar);
			// if it is not, it won't and the tile will return to normal
			else
				tiles[col][row].setIcon(getIcon(l.getElement(col, row)));
		}
		
		public void mouseClicked(MouseEvent evt) {
			tiles[col][row].setIcon(selectedIcon);
			l.insert(col, row, selectedChar);
		}
		
	}
	
	private class SelectionAdapter extends MouseAdapter {
		
		int num;
		
		public SelectionAdapter(int num) {
			this.num = num;
		}
		
		public void mouseClicked(MouseEvent evt) {
			selectedIcon = icons[num];
			selectedChar = characters[num];
		}
		
	}
	
	private class CopyAdapter extends MouseAdapter {
		
		public void mouseClicked(MouseEvent evt) {
			StringSelection ss = new StringSelection(l.toString());
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			c.setContents(ss, null);
		}
		
	}
	
	public Editor() {
		
		selectedChar = characters[0];
		selectedIcon = icons[0];
		
		setTitle("Level Creator");
		setIconImage(new ImageIcon(getClass().getResource("logo.png")).getImage());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		
		for(int i = 0; i < OPTIONS_AMOUNT; i++) {
			options[i] = new JLabel();
			options[i].setIcon(icons[i]);
			options[i].addMouseListener(new SelectionAdapter(i));
			optionPanel.add(options[i]);
		}
		
		JButton copier = new JButton("Copy level to clipboard");
		copier.addMouseListener(new CopyAdapter());
		
		copier.setBackground(Color.LIGHT_GRAY);
		copier.setForeground(Color.WHITE);
		copier.setFont(new Font("Tahoma", Font.BOLD, 12));
		copier.setFocusPainted(false);
		optionPanel.add(copier);
		
		
		tilePanel.setLayout(new java.awt.GridLayout(Level.ROWS, Level.COLS));
		
		for(int j = 0; j < Level.ROWS; j++) {
			for(int i = 0; i < Level.COLS; i++) {
				tiles[i][j] = new JLabel();
				tiles[i][j].setOpaque(false);
				tiles[i][j].setIcon(icons[0]);
				tiles[i][j].addMouseListener(new CustomMouseAdapter(i, j));
				tilePanel.add(tiles[i][j]);
			}
		}
		
		
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		
		layout.setHorizontalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
					.addComponent(tilePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(optionPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				)
			)
		);
		
		layout.setVerticalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
					.addGap(20)
					.addComponent(optionPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(tilePanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)

			)
		);
		
		pack();

		setVisible(true);
		
		
	}
	
	private ImageIcon getIcon(char c) {
		for(int i = 0; i < OPTIONS_AMOUNT; i++)
			if(characters[i] == c)
				return icons[i];
		return null;
	}
		
}
