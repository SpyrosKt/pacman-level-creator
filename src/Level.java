
public class Level {

	static final int COLS = 19;
	static final int ROWS = 22;
	
	private char[][] text = new char[COLS][ROWS];
	
	public Level() {
		for(int i = 0; i < COLS; i++)
			for(int j = 0; j < ROWS; j++)
				text[i][j] = ' ';
	}
	
	public String toString() {
		String s = new String();
		for(int j = 0; j < ROWS; j++)
			for(int i = 0; i < COLS; i++)
				s += text[i][j];
		return s;
	}
	
	public void insert(int col, int row, char c) {
		text[col][row] = c;
	}
	
	public char getElement(int col, int row) {
		return text[col][row];
	}
	
}
